# To run: 
# Without the -fopenmp flag: Jacobi - ./jacobi2D-no-omp N max_iter num_threads 
#                            Gauss-Seidel - ./gs2D-no-omp N max_iter num_threads
# With the -fopenmp flag:    Jacobi - ./jacobi2D-omp N max_iter num_threads 
#                            Gauss-Siedel - ./gs2D-omp N max_iter num_threads

EXECS = omp_solved2 omp_solved3 omp_solved4 omp_solved5 omp_solved6 jacobi2D-omp gs2D-omp jacobi2D-no-omp gs2D-no-omp
FLAGS = -fopenmp -O3

all: ${EXECS} hw2_pdf

omp_solved2: omp_solved2.c
	gcc -O3 -fopenmp omp_solved2.c -o omp_solved2

omp_solved3: omp_solved3.c
	gcc -O3 -fopenmp omp_solved3.c -o omp_solved3

omp_solved4: omp_solved4.c
	gcc -O3 -fopenmp omp_solved4.c -o omp_solved4

omp_solved5: omp_solved5.c
	gcc -O3 -fopenmp omp_solved5.c -o omp_solved5

omp_solved6: omp_solved6.c
	gcc -O3 -fopenmp omp_solved6.c -o omp_solved6

jacobi2D-omp: jacobi2d-omp.c
	gcc ${FLAGS} jacobi2d-omp.c -lm -lrt -o jacobi2D-omp

jacobi2D-no-omp: jacobi2d-omp.c
	gcc -O3 jacobi2d-omp.c -lm -lrt -o jacobi2D-no-omp

gs2D-omp: gs2D-omp.c
	gcc ${FLAGS} gs2D-omp.c -lm -lrt -o gs2D-omp

gs2D-no-omp: gs2D-omp.c
	gcc -O3 gs2D-omp.c -lm -lrt -o gs2D-no-omp

hw2_pdf: hw2.tex
	latexmk -pdf $^

clean: 
	rm -f ${EXECS}

clean_pdf: 
	latexmk -pdf -c hw2.tex

