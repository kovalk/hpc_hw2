/* 2d Jacobi to solve -(u_{xx}+u_{yy}) = f */

#include <stdio.h>
#include <stdlib.h>
#include <math.h> 
#include "util.h"
#include <string.h>
#ifdef _OPENMP
#include <omp.h>
#endif 

/* Compute residual */
double compute_residual(double *u, int N, double invhsq)
{
   int i; 
   double tmp, res = 0.0; 
   
#pragma omp parallel for default(none) shared(N,u,invhsq) private(i,tmp) reduction(+:res)
   for (i=1; i <= N*N; i++)
   {
      int pos = N+i+2*ceil(1.*i/N);
      tmp = invhsq*(4.*u[pos] - (u[pos+1]+u[pos-1]+u[pos+N+2]+u[pos-(N+2)]))-1.;
      res += tmp*tmp; 
   }
   return sqrt(res); 
}


int main(int argc, char * argv[])
{
   int i, N, iter, max_iters, threads; 

   if (argc != 4) 
   {
      fprintf(stderr, "Function needs number of discretization points in each dimension, maximum number of iterations, and number of threads as inputs! \n" );
      abort();
   }

   sscanf(argv[1], "%d", &N);
   sscanf(argv[2], "%d", &max_iters);
   sscanf(argv[3], "%d", &threads);

#pragma omp parallel num_threads(threads)
   {
#ifdef _OPENMP
      int my_threadnum = omp_get_thread_num(); 
      int numthreads = omp_get_num_threads(); 
#else
      int my_threadnum = 0; 
      int numthreads = 1; 
#endif
      printf("Hello, I am thread %d out of %d\n", my_threadnum, numthreads);
   }

   /* Vectors contain an extra border of ghost points corresponding to the boundary */
   double * u = (double *) calloc(sizeof(double), (N+2)*(N+2)); 
   double * unew = (double *) calloc(sizeof(double), (N+2)*(N+2)); 

   double h = 1.0/(N+1); 
   double hsq = h*h; 
   double invhsq = 1./hsq; 
   double res, res0, tol = 1e-5; 

   res0 = compute_residual(u,N,invhsq);
   printf("Iter %d: Residual: %g\n", 0, res0);
   res = res0; 
   
   /* Initialize u to be zero */
#pragma omp parallel for default(none) shared(N,u) num_threads(threads)
   for (i = 0; i < (N+2)*(N+2); i++)
   {
      u[i] = 0.;
   }


   /* Code for plotting 
   FILE *gnuplot = popen("gnuplot -persistent", "w");
   fprintf(gnuplot,"set terminal pdf\n");
   fprintf(gnuplot, "set output 'jacobi_its_25.pdf'\n");
   fprintf(gnuplot, "set xlabel 'Iteration'\n");
   fprintf(gnuplot, "set ylabel 'Residual norm'\n");
   fprintf(gnuplot, "set title '2D Jacobi'\n");
  
   fprintf(gnuplot, "plot '-'\n"); */

   /* timing */
   timestamp_type time1, time2;
   get_timestamp(&time1);

   /* Being jacobi iteration */
   for (iter = 0; iter < max_iters && res/res0 > tol; iter++)
   {
#pragma omp parallel for default(none) shared(N,unew,u,hsq) num_threads(threads)
      /* Iterate only over inner points */ 
      for (i = 1; i <= N*N; i++)
      {
         int pos = N+i+2*ceil(1.*i/N);
         unew[pos] = 0.25*(hsq+u[pos+1]+u[pos-1]+u[pos+N+2]+u[pos-(N+2)]);
      } 

     double *utemp;
     utemp = u; 
     u = unew; 
     unew = utemp; 

     //res = compute_residual(u,N,invhsq);
     //fprintf(gnuplot, "%d %g\n", iter, res);
 
     /* Calculate residual every 10 steps */
     if (0 == (iter % 10))
     {
        res = compute_residual(u,N,invhsq);
        printf("Iter %d: Residual: %g\n", iter, res);
     }
   }

   //fprintf(gnuplot, "e\n");
   //fflush(gnuplot);
   get_timestamp(&time2);
   double elapsed = timestamp_diff_in_seconds(time1,time2);
   printf("Time elapsed is %f seconds.\n", elapsed);

   free(u); 
   free(unew);

   return(0); 
   
}
