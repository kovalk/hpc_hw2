# README #

repository for HPC class, assignment 2 

after make

To run: 

Without the -fopenmp flag: 

Jacobi - ./jacobi2D-no-omp N max_iter num_threads 

Gauss-Seidel - ./gs2D-no-omp N max_iter num_threads



With the -fopenmp flag:   

Jacobi - ./jacobi2D-omp N max_iter num_threads 

Gauss-Siedel - ./gs2D-omp N max_iter num_threads