/******************************************************************************
* FILE: omp_bug2.c
* DESCRIPTION:
*   Another OpenMP program with a bug. 
* AUTHOR: Blaise Barney 
* LAST REVISED: 04/06/05 
******************************************************************************/
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) 
{
int nthreads, i,tid;
float total;

/*** Spawn parallel region ***/
#pragma omp parallel default(none) private(tid,i) shared(nthreads,total) 
/*** Want tid and i to be private, nthreads and total to be shared. Note: if reduction isn't used, then total must be a private variable ***/
  {
  /* Obtain thread number */
  tid = omp_get_thread_num();
  /* Only master thread does this */
  if (tid == 0) {
    nthreads = omp_get_num_threads();
    printf("Number of threads = %d\n", nthreads);
    }
  printf("Thread %d is starting...\n",tid);

  #pragma omp barrier

  /* do some work */
  total = 0.0;
  #pragma omp for schedule(static,10) reduction(+:total)
  for (i=0; i<1000000; i++) 
     total = total + i*1.0;


  printf ("Thread %d is done! Total= %e\n",tid,total); 
  
  #pragma omp barrier
 
  /* Just a note: No need to print the total multiple times as it is the same for each thread after the loop */
  if (tid == 0) { 
     printf("Global total is : %e\n",total);
  }

  } /*** End of parallel region ***/
}
